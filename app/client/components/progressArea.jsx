ProgressArea = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    // Loads items from the datasource and puts them on this.data.*keyname*
    getMeteorData(){
        return{
            zip: Session.get('zip'),
            city: Session.get('city'),
            stations: Session.get('nprStations')
        }
    },
    render() {

        if (Session.get('apiError')){
            <div id="info" className="error">
                <h1>Oh No</h1>
                <p>It looks like we are having problems connecting to the internet. Are you sure you have a signal?</p>
            </div>
        }

        else if ( this.data.stations === false){
            return(
                <div className="loading">
                    <div className="loader-inner ball-scale-multiple">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            )
        }

        else if ( this.data.stations === false && Session.get('nprStationError') === true){
            <div id="info" className="error">
                <h1>Oh No</h1>
                <p>Something has gone wrong and we cannot locate a station close to you. Are you sure you have enabled location services for this application?</p>
            </div>
        }

        else{

            let stationComponents = this.data.stations.map(function(stations) {
                return (
                    <ul className="station blocks-3 blocks-mobile-33">
                        <li>{stations.call}</li>
                        <li className="frequency text-centered">{stations.frequency}</li>
                    </ul>
                )
            });

            return(
                <div id="info">
                    <h1>You are currently near <br /> <strong>{this.data.city}</strong></h1>

                    <div id="stations">

                    {stationComponents}
                    </div>
                </div>
            )
        }

    }
});