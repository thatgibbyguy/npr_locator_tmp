Meteor.startup(function(){

    if (Meteor.isClient){
        Meteor.startup(function(){
            Session.set('geoLocateError', false);
            Session.set('position', false);
            Session.set('zip',false);
            Session.set('apiError', false);
            Session.set('nprStations',false);
            Session.set('nprStationError',false);
        });
    }

    if (Meteor.isCordova) {
        
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
        
        function onSuccess(position) {
            Session.set('position',position.coords);

            Meteor.call('getLocation',position.coords.latitude,position.coords.longitude,function(e,r){
                if (r) {
                    let apiResponse = EJSON.parse(r.content);
                    Session.set('zip',apiResponse.postalCodes[0].postalCode);

                    Meteor.call('getCity', position.coords.latitude,position.coords.longitude, function (e,r) {
                        if (r) {
                            let cityResponse = EJSON.parse(r.content)
                            Session.set('city',cityResponse.geonames[0].name)
                        };
                        if(e){
                            alert(e)
                        }
                    });

                    Meteor.call('nprApi', apiResponse.postalCodes[0].postalCode, function (e,r) {
                        if (r){
                            let nprResponse = EJSON.parse(r.content);
                            let stations = [];
                            // let stationMap = newMap();

                            for (let ii of nprResponse)
                                stations.push({'call': `${ii.call}`,'frequency':`${ii.frequency}`,'city':`${ii.address[1]}`,'logo':`${ii.logo}`})

                            Session.set('nprStations', stations);
                        }
                        if (e){
                            Session.set('nprStationError',true);
                        }
                    });

                }
                if(e){
                    Session.set('apiError', e);
                }
            });

        }

        function onError(error) {
            alert(error.message)
            Session.set('geoLocateError', error.message);
        }
    }
});