Meteor.methods({
	getLocation(lat,lon){
		return Meteor.http.call('GET',`http://api.geonames.org/findNearbyPostalCodesJSON?lat=${lat}&lng=${lon}&username=thatgibbyguy`)
	},
	getCity(lat,lon){
		return Meteor.http.call('GET',`http://api.geonames.org/findNearbyPlaceNameJSON?formatted=true&lat=${lat}&lng=${lon}&username=thatgibbyguy`)
	},
	nprApi(zip){
		return Meteor.http.call('GET',`http://api.npr.org/v2/stations/search/${zip}?apiKey=`)
	}
});
